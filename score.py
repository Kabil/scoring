#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from sys import argv

import numpy as np


# ========= Useful functions ==============
def read_array(filename):
<<<<<<< HEAD
    r"""Read a matrix from a file"""
=======
    """Read array and convert to 2d np arrays"""
>>>>>>> f30e0dd9a34d41feab7bb04b1952aa83923c38cf
    array = np.genfromtxt(filename, dtype=float)
    if len(array.shape) == 1:
        array = array.reshape(-1, 1)
    return array


def accuracy_metric(solution, prediction):
<<<<<<< HEAD
    """_summary_

    Args:
        solution (np.array):
        prediction (np.array):

    Returns:
        _type_: _description_
    """
=======
>>>>>>> f30e0dd9a34d41feab7bb04b1952aa83923c38cf
    if solution.size == 0.0 or prediction.size == 0.0:
        return 0.0
    correct_samples = np.all(solution == prediction, axis=1)

    return correct_samples.mean()


def _HERE(*args):
    h = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(h, *args)


def mse(prediction, solution):
    r"""Mean-square error.
    Works even if the target matrix has more than one column"""
    if solution.size == 0.0 or prediction.size == 0.0:
        return 0.0
    mse = np.sum((solution - prediction) ** 2, axis=1)
    print("coucou")
    return np.mean(mse)


# =============================== MAIN ========================================
if __name__ == "__main__":
    #### INPUT/OUTPUT: Get input and output directory names

    prediction_file = argv[1]
    solution_file = argv[2]
    score_file = open(_HERE("scores.txt"), "w")

    # # Extract the dataset name from the file name
    prediction_name = os.path.basename(prediction_file)

    # Read the solution and prediction values into numpy arrays
    solution = read_array(solution_file)
    prediction = read_array(prediction_file)

    # Compute the score prescribed by the metric file
    score = accuracy_metric(solution, prediction)
    print("score: ", score)
    print("======= (" + prediction_name + "): score(accuracy_metric)=%0.2f =======" % score)
    # Write score corresponding to selected task and metric to the output file
    score_file.write("accuracy_metric: %0.2f\n" % score)
    score_file.close()

    mse = mse(prediction, solution)
    print("mse: ", mse)
