import os
import sys
import unittest

import numpy as np

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from score import accuracy_metric, mse  # Import your metric functions


class TestMetrics(unittest.TestCase):
    def setUp(self):
        # Set up test data
        self.solution = np.array([[1, 0, 1], [0, 1, 0], [1, 1, 1]])
        self.correct_prediction = np.array([[1, 0, 1], [0, 1, 0], [1, 1, 1]])
        self.incorrect_prediction = np.array([[0, 1, 0], [1, 0, 1], [0, 0, 1]])
        self.empty_array = np.array([])

    def test_accuracy_metric(self):
        # Test accuracy with correct predictions
        self.assertEqual(accuracy_metric(self.solution, self.correct_prediction), 1.0)

        # Test accuracy with incorrect predictions
        self.assertEqual(accuracy_metric(self.solution, self.incorrect_prediction), 0.0)

        # Test accuracy with empty arrays
        self.assertEqual(accuracy_metric(self.empty_array, self.empty_array), 0.0)

    def test_mse_metric(self):
        # Test MSE with correct predictions
        self.assertEqual(mse(self.solution, self.correct_prediction), 0.0)

        # Test MSE with incorrect predictions
        self.assertEqual(mse(self.solution, self.incorrect_prediction), 8 / 3)

        # Test MSE with empty arrays
        self.assertEqual(mse(self.empty_array, self.empty_array), 0.0)


if __name__ == "__main__":
    unittest.main()
