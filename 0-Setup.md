# Environment setup

## Configure Git

To start using Git from your computer, you must enter your credentials to identify yourself as the author of your work. The full name and email address should match the ones you use in GitLab.

1. In your shell, add your full name:

    ```bash
    git config --global user.name "John Doe"
    ```

2. Add your email address:

    ```bash
    git config --global user.email "your_email_address@example.com"
    ```

3. To check the configuration, run:

    ```bash
    git config --global --list
    ```

    The `--global` option tells Git to always use this information for anything you do on your system. If you omit `--global` or use `--local`, the configuration applies only to the current repository.


## SSH key setup

GitLab uses the SSH protocol to securely communicate with Git. When you use SSH keys to authenticate to the GitLab remote server, you don’t need to supply your username and password each time. Given that Gitlab LISN (https://gitlab.lisn.upsaclay.fr/) restricts external connections via SSH (such as those using Wifi), so in this TP, you are going to use [HTTPS](#https-setup).

To clone the project code, you'll need to use Git. To do this, you first need to set up an SSH key pair and import it into your Gitlab account. To do this, follow the steps below:

If you do not have an existing SSH key pair, generate a new one:
1. Open a terminal.
    ```bash
    ssh-keygen -t ed25519 -C "<comment>"
    ```
2. Press `Enter`. Output similar to the following is displayed:
    ```bash
    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/user/.ssh/id_ed25519):
    ```
3. Accept the suggested filename and directory
4. Specify a passphrase (let empty):
    ```bash
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    ```

Next, add an SSH key to your GitLab account;
1. Copy the contents of your public key file
    ```bash
    cat ~/.ssh/id_ed25519.pub
    ```
2. Sign in to GitLab.
3. On the left sidebar, select your avatar.
4. Select Edit profile.
5. On the left sidebar, select SSH Keys.
6. Select Add new key.
7. In the Key box, paste the contents of your public key.
8. In the Title box, type a description, like `Gitlab workshop`.
9. Select Add key.

## HTTPS setup

Clone with HTTPS when you want to authenticate each time you perform an operation between your computer and GitLab. But you can avoid to enter your username and password every time by cloning with HTTPS using a token:

```bash
git clone https://<username>:<token>@gitlab.lisn.upsaclay.fr/[namespace]/scoring.git
```
You can create as many personal access tokens as you like.

1. On the left sidebar, select your avatar.
2. Select Edit profile.
3. On the left sidebar, select Access Tokens.
4. Select Add new token.
5. Enter a name (like **Gitlab Workshop Token**) and expiry date for the token (set to 365 days later than the current date).
6. Select `read_repository` and `write_repository` permissions.
7. Select Create personal access token.

Save the personal access token somewhere safe. After you leave the page, you no longer have access to the token.

## Setting up working groups

To simulate a team, we invite you to form groups of 4 people, trying to mix people from different laboratories. One member of the group will be "Project Leader", creating a fork in your personal namespace:
1. On the project’s homepage (https://gitlab.lisn.upsaclay.fr/asard/scoring), in the upper-right corner, select `Fork`
2. For Project URL, select the personal namespace your fork should belong to.
3. Select Fork project.

GitLab creates your fork, and redirects you to the new fork’s page.

## Setting up colloraborating workflows

The `main` branch stores the official release history, and the `develop` branch serves as an integration branch for features. The Project leader creates a `develop` branch locally and push it to the server:

* Using SSH if utilizing Ethernet connection:
    ```bash
    git clone git@serveur-gitlab.lisn.upsaclay.fr:[namespace]/scoring.git
    cd scoring
    ```

* Using HTTPS if utilizing Wifi connection:
    ```bash
    git clone https://<username>:<token>@gitlab.lisn.upsaclay.fr/[namespace]/scoring.git
    cd scoring
    ```

* Creating a `develop` branch 
    ```bash
    git branch develop
    ```

* Pushing the commits from your local `develop` branch to the `develop` branch in the remote repository
    ```bash
    git push origin develop
    ```

    `origin`: this is the default name Git gives to the remote repository from which your local repository was cloned (or the primary remote repository you're working with). It's like a nickname for the URL of the remote repository. You can have multiple remotes with different names, but `origin` is the conventional default.

    `develop`: this specifies the branch that you want to push.

This branch will contain the complete history of the project, whereas `main` will contain an abridged version. Other developers should now clone the central repository and switch to develop.

Now, add your collaborators to the project:
1. Select Manage > Members.
2. Select Invite members.
3. Add three peoples in your group to the project with following roles:
    * A developer: `Developer` role
    * A tester: `Developer` role
    * A code reviewer: `Maintainer` role

## Cloning the repository for collaborators:

1. Clone the repository

* Using SSH if utilizing Ethernet connection:
    ```bash
    git clone git@serveur-gitlab.lisn.upsaclay.fr:[namespace]/scoring.git
    cd scoring
    ```

* Using HTTPS if utilizing Wifi connection:
    ```bash
    git clone https://<username>:<token>@gitlab.lisn.upsaclay.fr/[namespace]/scoring.git
    cd scoring
    ```

The developer should create feature branchs from the latest `develop` branch.

## Resources and useful links
* SSH keys: https://docs.gitlab.com/ee/user/ssh.html
* Gitflow workflow: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
